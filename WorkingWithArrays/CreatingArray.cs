﻿using System;

namespace WorkingWithArrays
{
    public static class CreatingArray
    {
        public static int[] CreateEmptyArrayOfIntegers()
        {
            var i = Array.Empty<int>();
            return i;
        }

        public static bool[] CreateEmptyArrayOfBooleans()
        {
            var i = Array.Empty<bool>();
            return i;
        }

        public static string[] CreateEmptyArrayOfStrings()
        {
            var i = Array.Empty<string>();
            return i;
        }

        public static char[] CreateEmptyArrayOfCharacters()
        {
            var i = Array.Empty<char>();
            return i;
        }

        public static double[] CreateEmptyArrayOfDoubles()
        {
            var i = Array.Empty<double>();
            return i;
        }

        public static float[] CreateEmptyArrayOfFloats()
        {
            var i = Array.Empty<float>();
            return i;
        }

        public static decimal[] CreateEmptyArrayOfDecimals()
        {
            var i = Array.Empty<decimal>();
            return i;
        }

        public static int[] CreateArrayOfTenIntegersWithDefaultValues()
        {
            int[] i = new int[10];
            return i;
        }

        public static bool[] CreateArrayOfTwentyBooleansWithDefaultValues()
        {
            bool[] i = new bool[20];
            return i;
        }

        public static string[] CreateArrayOfFiveEmptyStrings()
        {
            string[] i = new string[5];
            return i;
        }

        public static char[] CreateArrayOfFifteenCharactersWithDefaultValues()
        {
            char[] i = new char[15];
            return i;
        }

        public static double[] CreateArrayOfEighteenDoublesWithDefaultValues()
        {
            double[] i = new double[18];
            return i;
        }

        public static float[] CreateArrayOfOneHundredFloatsWithDefaultValues()
        {
            float[] i = new float[100];
            return i;
        }

        public static decimal[] CreateArrayOfOneThousandDecimalsWithDefaultValues()
        {
            decimal[] i = new decimal[1000];
            return i;
        }

        public static int[] CreateIntArrayWithOneElement()
        {
            int[] i = new int[] { 123456 };
            return i;
        }

        public static int[] CreateIntArrayWithTwoElements()
        {
            int[] i = new int[] { 1111111, 9999999 };
            return i;
        }

        public static int[] CreateIntArrayWithTenElements()
        {
            int[] i = new int[] { 0, 4234, 3845, 2942, 1104, 9794, 923943, 7537, 4162, 10134 };
            return i;
        }

        public static bool[] CreateBoolArrayWithOneElement()
        {
            bool[] i = new bool[] { true };
            return i;
        }

        public static bool[] CreateBoolArrayWithFiveElements()
        {
            bool[] i = new bool[] { true, false, true, false, true };
            return i;
        }

        public static bool[] CreateBoolArrayWithSevenElements()
        {
            bool[] i = new bool[] { false, true, true, false, true, true, false };
            return i;
        }

        public static string[] CreateStringArrayWithOneElement()
        {
            string[] i = new string[] { "one" };
            return i;
        }

        public static string[] CreateStringArrayWithThreeElements()
        {
            string[] i = new string[] { "one", "two", "three" };
            return i;
        }

        public static string[] CreateStringArrayWithSixElements()
        {
            string[] i = new string[] { "one", "two", "three", "four", "five", "six" };
            return i;
        }

        public static char[] CreateCharArrayWithOneElement()
        {
            char[] i = new char[] { 'a' };
            return i;
        }

        public static char[] CreateCharArrayWithThreeElements()
        {
            char[] i = new char[] { 'a', 'b', 'c' };
            return i;
        }

        public static char[] CreateCharArrayWithNineElements()
        {
            char[] i = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'a' };
            return i;
        }

        public static double[] CreateDoubleArrayWithOneElement()
        {
            double[] i = new double[] { 1.12 };
            return i;
        }

        public static double[] CreateDoubleWithFiveElements()
        {
            double[] i = new double[] { 1.12, 2.23, 3.34, 4.45, 5.56 };
            return i;
        }

        public static double[] CreateDoubleWithNineElements()
        {
            double[] i = new double[] { 1.12, 2.23, 3.34, 4.45, 5.56, 6.67, 7.78, 8.89, 9.91 };
            return i;
        }

        public static float[] CreateFloatArrayWithOneElement()
        {
            float[] i = new float[] { 123456789.123456f };
            return i;
        }

        public static float[] CreateFloatWithThreeElements()
        {
            float[] i = new float[] { 1000000.123456f, 2223334444.123456f, 9999.999999f, };
            return i;
        }

        public static float[] CreateFloatWithFiveElements()
        {
            float[] i = new float[] { 1.0123f, 20.012345f, 300.01234567f, 4000.01234567f, 500000.01234567f };
            return i;
        }

        public static decimal[] CreateDecimalArrayWithOneElement()
        {
            decimal[] i = new decimal[] { 10000.123456M };
            return i;
        }

        public static decimal[] CreateDecimalWithFiveElements()
        {
            decimal[] i = new decimal[] { 1000.1234M, 100000.2345M, 100000.3456M, 1000000.456789M, 10000000.5678901M };
            return i;
        }

        public static decimal[] CreateDecimalWithNineElements()
        {
            decimal[] i = new decimal[] { 10.122112M, 200.233223M, 3000.344334M, 40000.455445M, 500000.566556M, 6000000.677667M, 70000000.788778M, 800000000.899889M, 9000000000.911991M };
            return i;
        }
    }
}
